//
//  School_Finder_Code_ChallengeTests.swift
//  School Finder Code ChallengeTests
//
//  Created by Edmund Holderbaum on 10/14/18.
//  Copyright © 2018 Dawn Trigger Enterprises. All rights reserved.
//

import XCTest
@testable import School_Finder_Code_Challenge

class School_Finder_Code_ChallengeTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func specializedSchoolWithOneProgramShouldReturnOneSchool() {
        let id = "13K430"
        SchoolsQueryClient.getSchoolBy(id: id, completion: { schools in
            assert(schools.count == 1)
        })
    }
    
    func specializedSchoolWithManyProgramsShouldReturnManySchools() {
        let id = "03M485"
        SchoolsQueryClient.getSchoolBy(id: id, completion: {schools in
            assert(schools.count == 6)
        })
    }
    
    func geSchoolWithOneProgramShouldReturnOneSchool() {
        let id = "28Q690"
        SchoolsQueryClient.getSchoolBy(id: id, completion: {schools in
            assert(schools.count == 1)
        })
    }
    
    func geSchoolWithManyProgramsShouldReturnManySchools() {
        let id = "20K490"
        SchoolsQueryClient.getSchoolBy(id: id, completion: {schools in
            assert(schools.count == 6)
        })
    }
}
