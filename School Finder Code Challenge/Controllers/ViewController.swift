//
//  ViewController.swift
//  School Finder Code Challenge
//
//  Created by Edmund Holderbaum on 10/14/18.
//  Copyright © 2018 Dawn Trigger Enterprises. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorArea: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var selectMode: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var schoolsByBorough: [Borough : [School]] = [:]
    var boroughs: [Borough] {
        return schoolsByBorough.keys.sorted(by: {$0.rawValue < $1.rawValue})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        activityIndicatorArea.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "schoolSegue" {
            let destVC = segue.destination as! OtherViewController
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            let boro = boroughs[indexPath.section]
            guard let schools = schoolsByBorough[boro] else { return }
            let school = schools[indexPath.row]
            destVC.school = school
        }
    }
    
    func getSchools() {
        guard let text = searchBar.text else {return}
        toggleActivityIndicator()
        switch selectMode.selectedSegmentIndex {
        case 1:
            SchoolsQueryClient.querySchoolsBy(idNumber: text, completion: updateWithSchools)
        case 2:
            SchoolsQueryClient.querySchoolsBy(district: text, completion: updateWithSchools)
        case 3:
            SchoolsQueryClient.getSchoolsBy(score: text, completion: updateWithSchools)
        default:
            SchoolsQueryClient.querySchoolsBy(name: text, completion: updateWithSchools)
        }
    }
    
    func updateWithSchools(_ schools: [School]) {
        schoolsByBorough = SchoolBuilder.sortByBorough(schools)
        toggleActivityIndicator()
        tableView.reloadData()
    }

    func toggleActivityIndicator() {
        let unhide = activityIndicatorArea.isHidden
        if unhide {
            activityIndicatorArea.isHidden = false
            activityIndicator.startAnimating()
        } else {
            activityIndicatorArea.isHidden = true
            activityIndicator.stopAnimating()
        }
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return schoolsByBorough.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return boroughs.map({$0.rawValue})[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let boro = boroughs[section]
        return schoolsByBorough[boro]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath)
        let boro = boroughs[indexPath.section]
        guard let schools = schoolsByBorough[boro] else { return cell }
        let school = schools[indexPath.row]
        cell.textLabel?.text = school.name
        if school.name != school.programName {
            cell.detailTextLabel?.text = school.programName
        } else {
            cell.detailTextLabel?.text = nil
        }
        return cell
    }
    
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        getSchools()
    }
}
