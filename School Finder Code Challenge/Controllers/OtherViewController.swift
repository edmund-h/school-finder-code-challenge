//
//  OtherViewController.swift
//  School Finder Code Challenge
//
//  Created by Edmund Holderbaum on 10/15/18.
//  Copyright © 2018 Dawn Trigger Enterprises. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var mathAvg: UILabel!
    @IBOutlet weak var readingAvg: UILabel!
    @IBOutlet weak var writingAvg: UILabel!
    
    let fieldTitleAttributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18)
    ]
    let fieldTextAttributes: [NSAttributedString.Key : Any] = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)
    ]
    
    let spacer = NSAttributedString(string:"\n\n")
    
    var school: School?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        if school?.avgSATScores == nil,
            var school = self.school{
            SATsQueryClient.getScoreFor(school.id, completion: { scores in
                guard let first = scores.first else {return}
                school.avgSATScores = first
                self.mathAvg.text = first.avgMath
                self.readingAvg.text = first.avgReading
                self.writingAvg.text = first.avgWriting
            })
        }
        textView.isEditable = false
    }
    
    func setup() {
        guard let school = school else { return }
        
        titleLabel.text = school.name
        addressLabel.text = school.address
        
        if let scores = school.avgSATScores {
            mathAvg.text = scores.avgMath
            readingAvg.text = scores.avgReading
            writingAvg.text = scores.avgWriting
        }
        
        //this looks really awful in code, was terrible to build, but looks pretty good in the app. there are probably better ways to do this
        
        let mutableString = NSMutableAttributedString(string: "")
        
        let overviewTitle = NSAttributedString(string: "Overview:\n", attributes: fieldTitleAttributes)
        let overviewString = NSAttributedString(string: school.overview, attributes: fieldTextAttributes)
        mutableString.append(overviewTitle)
        mutableString.append(overviewString)
        mutableString.append(spacer)
        if let academicOpportunities = school.academicOpportunities {
           let aO = NSAttributedString(string: academicOpportunities, attributes: fieldTextAttributes)
            let aOTitle = NSAttributedString(string: "\nAcademic Opportunities: ", attributes: fieldTitleAttributes)
                mutableString.append(aOTitle)
                mutableString.append(aO)
        }
        if let ellPrograms = school.ellPrograms {
            let ellProgramsTitle = NSAttributedString(string: "ELL Programs: ", attributes: fieldTitleAttributes)
            let ellStr = NSAttributedString(string: ellPrograms, attributes: fieldTextAttributes)
            mutableString.append(ellProgramsTitle)
            mutableString.append(ellStr)
        }
        if let languageClasses = school.languageClasses {
            let languageClassesTitle = NSAttributedString(string: "\nLanguage Classes: ", attributes: fieldTitleAttributes)
            let languageStr = NSAttributedString(string: languageClasses, attributes: fieldTextAttributes)
            mutableString.append(languageClassesTitle)
            mutableString.append(languageStr)
        }
        if let startTime = school.startTime {
            let startTimeTitle = NSAttributedString(string: "\nStart Time: ", attributes: fieldTitleAttributes)
            let startStr = NSAttributedString(string: startTime, attributes: fieldTextAttributes)
            mutableString.append(startTimeTitle)
            mutableString.append(startStr)
        }
        if let endTime = school.endTime {
            let endTimeTitle = NSAttributedString(string: "\nEnd Time: ", attributes: fieldTitleAttributes)
            let endStr = NSAttributedString(string: endTime, attributes: fieldTextAttributes)
            mutableString.append(endTimeTitle)
            mutableString.append(endStr)
        }
        if let apCourses = school.apCourses {
            let apCoursesTitle = NSAttributedString(string: "\nAP Courses: ", attributes: fieldTitleAttributes)
            mutableString.append(apCoursesTitle)
            let apStr = NSAttributedString(string: apCourses, attributes: fieldTextAttributes)
            mutableString.append(apStr)
        }
        if let addtl = school.additionalInfo {
        let additionalInfoTitle = NSAttributedString(string: "\nAdditional Information: ", attributes: fieldTitleAttributes)
            let addtlStr = NSAttributedString(string: addtl, attributes: fieldTextAttributes)
            mutableString.append(additionalInfoTitle)
            mutableString.append(addtlStr)
        }
        mutableString.append(spacer)
        
        let totalStudentsTitle = NSAttributedString(string: "Total Students: ", attributes: fieldTitleAttributes)
        let totalStr = NSAttributedString(string: school.totalStudents, attributes: fieldTextAttributes)
        mutableString.append(totalStudentsTitle)
        mutableString.append(totalStr)
        if let girls = school.girls {
        let girlsTitle = NSAttributedString(string: "Girls: ", attributes: fieldTitleAttributes)
            let girlStr = NSAttributedString(string: girls, attributes: fieldTextAttributes)
            mutableString.append(girlsTitle)
            mutableString.append(girlStr)
        }
        if let boys = school.boys {
            let boysTitle = NSAttributedString(string: "Boys: ", attributes: fieldTitleAttributes)
            mutableString.append(boysTitle)
            let boyStr = NSAttributedString(string: boys, attributes: fieldTextAttributes)
            mutableString.append(boyStr)
        }
        let graduationRateTitle = NSAttributedString(string: "\nGraduation Rate: ", attributes: fieldTitleAttributes)
        let grads = NSAttributedString(string: school.graduationRate, attributes: fieldTextAttributes)
        mutableString.append(graduationRateTitle)
        mutableString.append(grads)
        mutableString.append(spacer)
        
        let councilDistrictTitle = NSAttributedString(string: "\nSchool Board District: ", attributes: fieldTitleAttributes)
        let councilStr = NSAttributedString(string: school.councilDistrict, attributes: fieldTextAttributes)
        mutableString.append(councilDistrictTitle)
        mutableString.append(councilStr)
        let neighborhoodTitle = NSAttributedString(string: "\nNeighborhood: ", attributes: fieldTitleAttributes)
        let neighborhood = NSAttributedString(string: school.neighborhood, attributes: fieldTextAttributes)
        mutableString.append(neighborhoodTitle)
        mutableString.append(neighborhood)
        if let phoneNumber = school.phoneNumber{
        let phoneNumberTitle = NSAttributedString(string: "\nPhone Number: ", attributes: fieldTitleAttributes)
        let phoneStr = NSAttributedString(string: phoneNumber, attributes: fieldTextAttributes)
            mutableString.append(phoneNumberTitle)
            mutableString.append(phoneStr)
        }
        if let email = school.email {
        let emailTitle = NSAttributedString(string: "\nSchool Email: ", attributes: fieldTitleAttributes)
            let emailStr = NSAttributedString(string: email, attributes: fieldTextAttributes)
            mutableString.append(emailTitle)
            mutableString.append(emailStr)
        }
        if let website = school.website {
        let websiteTitle = NSAttributedString(string: "\nSchool Website: ", attributes: fieldTitleAttributes)
            let webStr = NSAttributedString(string: website, attributes: fieldTextAttributes)
            mutableString.append(websiteTitle)
            mutableString.append(webStr)
        }
        if let subways = school.subway {
        let subwayTitle = NSAttributedString(string: "\nNearby Subways:\n", attributes: fieldTitleAttributes)
            let subwayStr = NSAttributedString(string: subways, attributes: fieldTextAttributes)
            mutableString.append(subwayTitle)
            mutableString.append(subwayStr)
        }
        if let buses = school.bus {
        let busTitle = NSAttributedString(string: "\nNearby Busses: ", attributes: fieldTitleAttributes)
            let busStr = NSAttributedString(string: buses, attributes: fieldTextAttributes)
            mutableString.append(busTitle)
            mutableString.append(busStr)
        }
        mutableString.append(spacer)
        if let extra = school.extracurricularActivities {
            let extraTitle = NSAttributedString(string: "\nExtracurricular Activities:\n", attributes: fieldTitleAttributes)
            let extraStr = NSAttributedString(string: extra, attributes: fieldTextAttributes)
            mutableString.append(extraTitle)
            mutableString.append(extraStr)
        }
        if let sportB = school.sportsBoys {
        let sportsBoysTitle = NSAttributedString(string: "\nBoys' PSAL Sports: ", attributes: fieldTitleAttributes)
            let sportBStr = NSAttributedString(string: sportB, attributes: fieldTextAttributes)
            mutableString.append(sportsBoysTitle)
            mutableString.append(sportBStr)
        }
        if let sportG = school.sportsGirls {
        let sportsGirlsTitle = NSAttributedString(string: "\nGirls' PSAL Sports: ", attributes: fieldTitleAttributes)
            let sportGStr = NSAttributedString(string: sportG, attributes: fieldTextAttributes)
            mutableString.append(sportsGirlsTitle)
            mutableString.append(sportGStr)
        }
        if let sportC = school.sportsCoed {
        let sportsCoedTitle = NSAttributedString(string: "\nCoed PSAL Sports: ", attributes: fieldTitleAttributes)
            let sportCStr = NSAttributedString(string: sportC, attributes: fieldTextAttributes)
            mutableString.append(sportsCoedTitle)
            mutableString.append(sportCStr)
        }
        mutableString.append(spacer)
        
        if let programName = school.programName,
            let programDescription = school.programDescription,
            let programMethod = school.programApplicationMethod,
            let programSeats = school.programSeats,
        let programApplicants = school.programApplicants {
        let programNameTitle = NSAttributedString(string: "\nProgram Name: ", attributes: fieldTitleAttributes)
        let nameStr = NSAttributedString(string: programName, attributes: fieldTextAttributes)
            let programDescriptionTitle = NSAttributedString(string: "\nProgram Description:\n", attributes: fieldTitleAttributes)
            let descStr = NSAttributedString(string: programDescription, attributes: fieldTextAttributes)
        let programSeatsTitle = NSAttributedString(string: "\nNumber of Seats: ", attributes: fieldTitleAttributes)
           let seatStr = NSAttributedString(string: programSeats, attributes: fieldTextAttributes)
        let programApplicantsTitle = NSAttributedString(string: "\nNumber of Applicants: ", attributes: fieldTitleAttributes)
            let applStr = NSAttributedString(string: programApplicants, attributes: fieldTextAttributes)
        let programApplicationMethodTitle = NSAttributedString(string: "\nHow to Apply: ", attributes: fieldTitleAttributes)
            let methodStr = NSAttributedString(string: programMethod, attributes: fieldTextAttributes)
            
            mutableString.append(programNameTitle)
            mutableString.append(nameStr)
            mutableString.append(programDescriptionTitle)
            mutableString.append(descStr)
            mutableString.append(programApplicationMethodTitle)
            mutableString.append(methodStr)
            mutableString.append(programSeatsTitle)
            mutableString.append(seatStr)
            mutableString.append(programApplicantsTitle)
            mutableString.append(applStr)
        }
        
        textView.attributedText = mutableString
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
